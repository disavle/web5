function onClick() {
    let i1 = document.getElementById("input1").value;
    let i2 = document.getElementById("input2").value;
    console.log(i1, i2);
    let r = document.getElementById("result");
    if((i1.match(/^[0-9]*[.]?[0-9]+$/) === null) || (i2.match(/^[0-9]*[.]?[0-9]+$/) === null)){
        r.innerHTML = "Некорректно";
        alert("Введите числа (числа > 0, кол-во - натуральное число)");
    }
    else {
        r.innerHTML = (i1*i2).toFixed(2);
        return false;
    }
}

window.addEventListener("DOMContentLoaded", function (event) {
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("button");
    b.addEventListener("click", onClick);
});
